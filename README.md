# Ansible Role Openstack [![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-yellow.svg)](https://conventionalcommits.org)

Role for installing openstack on Ubuntu.

## Requirements
This role requires Ansible 2.6 or higher, and platform requirements are listed in the metadata file.

## Installation

add this in **requirements.yml**

```shell
- src: git@gitlab.com:araulet-team/devops/ansible/ansible-role-openstack.git
  name: araulet.openstack
```

## Role Variables
The variables that can be passed to this role and a brief description about them are as follows:

```yaml
roles:
  - role: araulet.openstack
    become: true
    vars:
      openstack_install: true
      openstack_version: v2.11.0
    tags: [ 'openstack' ]
```
## Dependencies

None

## Licence
MIT

## Author Information
Arnaud Raulet